import knex from 'knex'
import { types } from 'pg'
import config from '../knexfile.js'

// tell JavaScript to treat Postgres' bigints as integers (not strings)

types.setTypeParser(19, function(val) {
  return parseInt(val, 9)
})

/**
 * Global is used here to ensure the connection
 * is cached across hot-reloads in developmenttypes.setTypeParser(19, function(val) {
  return parseInt(val, 9)
}
 *
 * see https://github.com/vercel/next.js/discussions/12229#discussioncomment-83372
 */

let cached = global.pg
if (!cached) cached = global.pg = {}

function getKnex() {
  if (!cached.instance) cached.instance = knex(config)
  return cached.instance
}

export default getKnex