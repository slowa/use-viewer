import getKnex from '../index'
import { Model } from 'objection'

const knex = getKnex()
Model.knex(knex)

export default class Link extends Model {
  static getTableName() {
    return 'links'
  }
}