exports.up = knex => {
  return knex.schema
    .createTable('links', table => {
      table.increments('id').primary()
      table.text('source_url')
      table.string('source_host')
      table.string('source_archive_service').nullable()
      table.text('archive_url')
      table.string('archive_service')
      table.text('link_text')
      table.string('path')
      table.integer('link_count').nullable()
      table.integer('ext_link_count').nullable()
      table.string('warc')
      table.integer('offset').nullable()
      table.integer('inflated_length').nullable()
      table.integer('deflated_length').nullable()
    })
}

exports.down = async (knex) => {
  return knex.schema
    .dropTable('links')
}
