FROM node:current

ENV PORT 3000

RUN mkdir -p /app
WORKDIR /app

COPY . /app
RUN yarn install

CMD "./start.sh"