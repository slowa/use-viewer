const fs = require('fs')
const csv = require('csv')
const knex = require('knex')
const path = require('path')
const knexfile = require('../knexfile')
const archive = require('../utils/archive')

async function main() {

  const db = knex(knexfile)
  const linkCount = await db('links').count('id')

  if (linkCount[0].count > 0) {
    console.log('links table already has data, not importing links.csv')
  } else {
    const csvFile = path.resolve(__dirname, '..', 'public', 'data', 'links.csv')
    const parse = csv.parse({columns: true})
    const input = fs.createReadStream(csvFile)
    input.pipe(parse)

    // batch insert every 100 rows from the csv

    let rows = []
    for await (const row of parse) {

      // this was unfortunately null in the first downloaded data
      // removing it allows the row to be added
      delete row['deflated_length']

      // add a column indicating whether the source url is from an archive
      row['source_archive_service'] = archive.archiveService(row['source_url'])

      rows.push(row)
      if (rows.length > 100) {
        console.log('inserting', rows.map(r => r.source_url))
        const ids = await db.insert(rows, 'id').into('links')
        rows = []
      }
    }
  }

  db.destroy()
}

main()