import Link from 'next/link'
import useSWR from 'swr'

const fetcher = (url) => fetch(url).then((res) => res.json())

export default function Home() {
  const { data, error } = useSWR('/api/hosts', fetcher)

  if (error) return <div>Failed to load hosts!</div>
  if (!data) return <div>Loading...</div>

  return (
    <section>
      <h2>Source Hosts</h2>
      <p summary>
        This table lists <em>Source Hosts</em> or website host names in
        the <a href="https://commoncrawl.org">Common Crawl</a> dataset whose pages link to a known
        web archive service. The total links found in the dataset is in the <em>Archive Links</em> column.
        Selecting  
 
 
 one of the host names will bring you to a detailed report for it.
      </p>
      <table>
        <thead>
          <tr>
            <td>Source Host</td>
            <td className="right">Archive Links</td>
          </tr>
        </thead>
        <tbody>
        {data.map(h => (
          <tr key={h.sourceHost}>
            <td>{h.sourceHost}</td>
            <td className="right">{h.total.toLocaleString()}</td>
          </tr>
        ))}
        </tbody>
      </table>
    </section>
  )
}