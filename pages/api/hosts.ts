import Link from '../../knex/models/link'

export default async (req, res) => {
  const results = await Link.query()
    .select('sourceHost')
    .where('sourceArchiveService', null)
    .count('* as total')
    .groupBy('sourceHost')
    .orderBy('total', 'DESC')
    .limit(100)

  res.statusCode = 200
  res.json(results)
}