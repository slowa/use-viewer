import React from 'react'
import Head from 'next/head'
import Image from 'next/image'

import '../styles/globals.css'

export default function MyApp({ Component, pageProps }) {
  return (
    <React.Fragment>
      <Head>
        <title>SloWA</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
     <div className="container">
        <header>
          <Image width="50" height="50" src="/images/all-the-things.jpg" />
          <br />
          SLoWA
        </header>
        <Component {...pageProps} />
      </div>
    </React.Fragment>
  )
}