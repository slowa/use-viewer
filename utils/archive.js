const path = require('path')
const URL = require('url').URL
const archives = require('../public/data/archives.json')

/**
 * Determine the archive service for a URL.
 * 
 * @param {*} url 
 */

function archiveService(url) {
  try { 
    const u = new URL(url)
    return archives[u.host.toLowerCase()] 
  } catch (e) {
    if (e instanceof TypeError) {
      return undefined
    } else {
      throw e
    }
  }
}

/**
 * Determine the  
 * @param {*} url 
 */

function isArchive(url) {
  return archiveService(url) ? true : false
}

module.exports = {
  archiveService,
  isArchive
}