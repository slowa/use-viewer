# use-viewer

A web viewer for data collected with [commoncrawl-use] data.

## Install

It's probably easiest to install & run this web app using [docker-compose]:

    cp .env-example .env
    docker-compose build --no-cache
    docker-compose

[commoncrawl-use]: https://gitlab.com/slowa/commoncrawl-use
[docker-compose]: https://docs.docker.com/compose/install/
